
var http = require('http')
var server = http.createServer()


function start() {
	function onRequest(request, response) {
		console.log("Requête reçue")
		response.writeHead(200, {"Content-Type": "text/plain"})
		response.write("Hello")
		response.end()
	}
	http.createServer(onRequest).listen(3030)
	console.log("démarrage du serveur.")
}

exports.start = start

